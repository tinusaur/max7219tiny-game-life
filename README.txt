Game-Life - This is the Conway's Game of Life

-------------------------------------------------------------------------------
 Copyright (c) 2022 Tinusaur (https://tinusaur.com). All rights reserved.
 Distributed as open source under the MIT License (see the LICENSE.txt file).
 Please, retain in your work a link to the Tinusaur project website.
-------------------------------------------------------------------------------

This is Conway's Game of Life

It is intended to be used with the Tinusaur boards but should also work with any other board based on ATtiny85 or a similar microcontroller.

==== Links ====

Official Tinusaur website: http://tinusaur.com
Game-Life Source Code: https://gitlab.com/tinusaur/max7219tiny-game-life

Twitter: https://twitter.com/tinusaur
Facebook: https://www.facebook.com/tinusaur

